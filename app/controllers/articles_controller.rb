class ArticlesController < ApplicationController

  def new
      # @article = Article.new
      # render plain: params[:article].inspect
  end

  def index
    @article = Article.all
  end

  def show
   @article = Article.find(params[:id])
  end

  def create
    @article = Article.new(article_params)
     if @article.valid?
       @article.save
       # render plain: "OK"
        redirect_to @article
     else
       render action: 'new'
     end
  end

  def edit
      @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    if @article.update(article_params)
        # redirect_to @article #Переводит на articles#show
        render plain: "OK"
    else
      render action: 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy

    redirect_to articles_path
  end
  private

  def article_params
   params.require(:article).permit(:title, :text)
  end
end
