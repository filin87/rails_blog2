Rails.application.routes.draw do
  get 'pages/terms'
  get 'pages/about'
  get 'home/index'
  get 'contacts' => 'contacts#new'
  get 'terms' => 'pages#terms'
  resources :articles do
    resources :comments
  end
  resource :contacts, only: [:create] #Добавляем для контроллера маршруты только для указанных методов
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
